import tweepy
import time
import random

# Import the file containing my Twitter app keys
from keys import *

print("This is my twitter bot")

# Authentication
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_KEY, ACCESS_SECRET)

# The api object to interact with Twitter; read & write data from Twitter
api = tweepy.API(auth)

"""
-----------METHODS-------------
1. reply_to_mentions() - replies to the tweets mentioned to my account with #MotivateMe keyword
2. get_quote() - opens a file in read mode, stores its lines in a list then returns an element from the list at random

"""


def get_quote():
    f_read = open('famous-quotes.txt', 'r')
    data = f_read.readlines()
    rnd = random.randint(0, len(data))
    return data[rnd]


# Method to reply to all the mentions
def reply_to_mentions():
    print("retrieving and replying to tweets...")

    # mentions_timeline() method gets all the tweets mentioned to our account
    # "extended" mode to include the full text of long tweets
    mentions = api.mentions_timeline(tweet_mode='extended')

    # reversed() because mentions to our account form a list
    # and every new mention is appended to the list, so the
    # latest tweet would be found at the last
    for mention in reversed(mentions):
        print(str(mention.id) + mention.full_text)
        if "#motivateme" in mention.full_text.lower():
            print("Found #motivateme")
            print("responding back...")
            quote = get_quote()
            api.update_status(
                "@" + mention.user.screen_name + " " + quote)


while True:
    reply_to_mentions()
    time.sleep(60)
