We are what we repeatedly do. Excellence, therefore, is not an act but a habit.
The best way out is always through.
Do not wait to strike till the iron is hot; but make it hot by striking.
Great spirits have always encountered violent opposition from mediocre minds.
Whether you think you can or think you can’t, you’re right.
I know for sure that what we dwell on is who we become.
You must be the change you want to see in the world.
What you get by achieving your goals is not as important as what you become by achieving your goals.
You can get everything in life you want if you will just help enough other people get what they want.
Whatever you do will be insignificant, but it is very important that you do it.
Desire is the starting point of all achievement, not a hope, not a wish, but a keen pulsating desire which transcends everything.
Failure is the condiment that gives success its flavor.
Vision without action is daydream. Action without vision is nightmare.
In any situation, the best thing you can do is the right thing; the next best thing you can do is the wrong thing; the worst thing you can do is nothing.
If you keep saying things are going to be bad, you have a chance of being a prophet.
Success consists of doing the common things of life uncommonly well.
Keep on going and the chances are you will stumble on something, perhaps when you are least expecting it. I have never heard of anyone stumbling on something sitting down.
Losers visualize the penalties of failure. Winners visualize the rewards of success.
Some succeed because they are destined. Some succeed because they are determined.
A happy person is not a person in a certain set of circumstances, but rather a person with a certain set of attitudes.
If you’re going to be able to look back on something and laugh about it, you might as well laugh about it now.
Remember that happiness is a way of travel, not a destination.
If you want to test your memory, try to recall what you were worrying about one year ago today.
What lies behind us and what lies before us are tiny matters compared to what lies within us.
We judge of man’s wisdom by his hope.